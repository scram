/**
 * 
 */
package scram.backend.model



/**
 * @author kfr
 *
 */
public class Sprint  extends YamlObject{
	
	String name

	def storyBoxes;
	
	def attributeMap
	
	def getValue(key) {
		return attributeMap.get(key)
	}
}
