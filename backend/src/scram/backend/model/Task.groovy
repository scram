/**
 * 
 */
package scram.backend.model



/**
 * @author kfr
 *
 */
public class Task extends YamlObject{

	def attributeMap
	
	def getValue(key) {
		return attributeMap.get(key)
	}
	
}
