/**
 * 
 */
package scram.backend.model



/**
 * @author kfr
 *
 */
public class Project  extends YamlObject{
	
	String name 
	String productOwner
	
	def sprints

	def attributeMap
	
	def getValue(key) {
		return attributeMap.get(key)
	}
}
