/**
 * 
 */
package scram.backend.model



/**
 * @author kfr
 *
 */
public class YamlObject{

	def attributeMap
	
	def getValue(key) {
		return attributeMap.get(key)
	}
}
