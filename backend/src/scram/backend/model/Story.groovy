/**
 * 
 */
package scram.backend.model



/**
 * @author kfr
 *
 */
public class Story extends YamlObject{

	def tasks;
	
	def title;
	def details; 
	def originalEstimate; 
	def	burndown;
	def implementor; 
	def template;
	def location;

	def attributeMap
	
	def getValue(key) {
		return attributeMap.get(key)
	}
}
