/**
 * 
 */
package scram.backend.model



/**
 * @author kfr
 *
 */
public class Box  extends YamlObject{

	String name
	
	def stories
	
	def attributeMap
	
	def getValue(key) {
		return attributeMap.get(key)
	}
}
