package scram.backend

import org.ho.yaml.Yaml
import org.ho.yaml.YamlStream
import scram.backend.model.*
import groovy.swing.factory.BoxFactory
public class Backend {
	
	def dirName

def Project[] listProjects(final dir) {
	
	def projects = new ArrayList();
	if(dir.endsWith("/")) {
		dirName = dir 
	} else {
		dirName = dir + "/"  
	}

	new File(dirName).eachFile {
		file ->
		def project
		project = loadProject(file.getName()+"/") 
		projects.add(project)
	}
	return projects;
}

def Project loadProject(filename) {
	def projectFilename
	if(dirName == null) {
		dirName = ""
	}
	
	projectFilename = filename + "project.yaml" 

	File f = new File(dirName + projectFilename)
	def project = getProject(f)

	def sprints = new ArrayList()
	new File(dirName + filename).eachFile {
		file ->
		File f2 = new File (dirName + filename + file.getName())
		if(f2.isDirectory()) {
			def sprint
			sprint = loadSprint(f2)
			sprints.add(sprint)
		}
	}
	project.sprints = sprints
	
	return project
}
	
def Sprint loadSprint(f2) {

	def sprint
	f2.eachFile { 
		file ->
		File f = new File(f2.getAbsolutePath() + "/"+ file.getName())
		if(f.isFile()) {
			sprint = getSprint(f)
		}
	}
	
	sprint = loadBoxes(sprint, f2)
	return sprint
}

def Sprint loadBoxes(sprint, boxFile) {
	
	def storyBoxes = new ArrayList()
	
	boxFile.eachFile {
		file -> 
		File boxDir = new File(boxFile.getAbsolutePath() + "/" + file.getName())
		if(boxDir.isDirectory()) {
			def box = new Box(name:file.getName())
			def stories = new ArrayList()
			boxDir.eachFile {
				file2 -> 
				def story = getStory(new File(boxDir.getAbsolutePath() + "/" + file2.getName()))
				stories.add(story)
			}
			storyBoxes.add(box)
		}
	}
	
	return sprint
}

def Story getStory(f) {

	def story = Yaml.loadType(f, Story)	
	story.attributeMap = getAttributes(f)
	println story.attributeMap
	return story 
}

def writeStory(path, filename, story) {

	def absoluteFilename
	if(!filename.endsWith(".yaml")) {
		filename = filename + ".yaml"
	}
	if(path.endsWith("/")) {
		absoluteFilename = path + filename
	} else {
		absoluteFilename = path + "/" + filename
	}
	
	Yaml.dump(story.attributeMap, new File(absoluteFilename))
}

def Sprint getSprint(f) {

	def sprint = Yaml.loadType(f, Sprint)	
	sprint.attributeMap = getAttributes(f)
	return sprint 
}

def Project getProject(f) {

	def project = Yaml.loadType(f, Project)	
	project.attributeMap = getAttributes(f)
	return project
}

def Map getAttributes(f) {
	FileInputStream fis = new FileInputStream(f)
	YamlStream stream = Yaml.loadStream(fis)

	def map
	for (Object obj: stream){
	     map = obj
    }
	return map
}

}

