/**
 * 
 */
package scram.backend

import junit.framework.TestCase
import scram.backend.model.Project
import scram.backend.model.Story


/**
 * @author kfr
 *
 */
public class Test<Object> extends TestCase{
	
	void setUp() throws Exception{
	}
	
	void tearDown() throws Exception{
	}
	
	void testListProjects(){
		Backend b = new Backend()
		def projects = b.listProjects("/projects/scram/test/projects/")
		projects.each { project -> println "Project name : ${project.name}"}

		
	}
	
	void testLoadProject(){
		Backend b = new Backend()
		def project = b.loadProject("/projects/scram/test/projects/scram/")
		project.sprints.each {sprint -> println "${sprint.name}"}
	}
	
	void testLoadStory(){
		fail("Not yet implemented")
	}

	void testWriteStory(){

		def story = new Story()
		def map = ["template":"Bl� seddel", "originalEstimate":1, "title":"skrevet fra test", "details":"intet", "burndown":7, "implementor":"Jens Hansen"]
		story.attributeMap = map
		
		Backend b = new Backend()
		b.writeStory("/projects/scram/test/projects/scram/Backlog", "TestStory", story)
	}
}
