/*
 * Main.fx
 *
 * Created on Oct 29, 2008, 12:00:39 PM
 */

package scram;

/**
 * @author jhn
 */
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.image.*;
import javafx.scene.effect.*;
import javafx.scene.transform.*;
import javafx.ext.swing.*;
import javafx.scene.effect.*;
import javafx.scene.effect.light.*;
import javafx.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.animation.*;
import scram.pages.SprintList;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

var title = "noget";

var windows : Node[] = [];
var displayedComponents : Component[] = [];
var size = new WindowSize();

var sprintList : Component = SprintList.create([ "test1", "test2", "test3"], showPage, size);
var c : Component = sprintList;

function showPage(component : Component) : Void {
    var current = c;
    var height = 100.0;
    var l = PageIcon {
        height: height
        savedComponent: current
        action: function(comp:Component) : Void {
            showPage(comp);
            c.visible = true;
        }
    };
    
    if (javafx.lang.Sequences.indexOf(displayedComponents, current) == -1) {
        insert l into windows;
        insert current into displayedComponents;
    }
    
    if (c != component) {
        c.visible = false;
    }
    c = component;
}

function closePage(component : Component) : Void {
    var idx : Integer = javafx.lang.Sequences.indexOf(displayedComponents, component);
    delete windows[idx];
    delete displayedComponents[idx];
}

var f : SwingFrame = SwingFrame {
    title: "Scram: " + title
    width: GUIUtils.getWindowSize().width
    height: GUIUtils.getWindowSize().height
    x: 0 y: 0
    menus: [
        Menu {
            text: "File"
            items : [
                MenuItem {
                    text: "Quit"
                    action: function() {
                        java.lang.System.exit(0);
                    }
                },
                MenuItem {
                    text: "Reload"
                    action: function() {
                        closePage(sprintList);
                        sprintList = SprintList.create([ "test4", "test353453", "tesaskljht3"], showPage, size);
                        showPage(sprintList);
                    }
                }
            ]
        }
    ]

    content: 
        Panel {
            content: [
                BorderPanel {
                    center: bind c
                    width: bind size.width
                    height: bind size.height - 101
                },
                
                Canvas {                
                    height: 101
                    width: bind size.width
                    x: 0
                    y: bind size.height - 101
                    content: [
                        Line {
                            startX: 0
                            startY: 0
                            endX: 10000
                            endY: 0
                            stroke: Color.BLACK
                            effect: DropShadow {}
                        },
                        HBox {
                            translateY: 1
                            translateX: bind size.width / 2 - (sizeof windows * 105) / 2
                            //effect: Lighting { light: PointLight { x: 100 y: 100 z: 60 }}
                            spacing: 10
                            content: bind windows
                        }
                        ]

                }]
            }
}

var listener = Listener {
    size: size
    frame: f
}
f.getJFrame().addComponentListener(listener);
f.visible = true;
