/*
 * WindowSize.fx
 *
 * Created on Oct 30, 2008, 8:36:50 AM
 */

package scram;

/**
 * @author recht
 */

public class WindowSize {
    public attribute width : Integer;
    public attribute height : Integer;
    public attribute x : Integer;
    public attribute y : Integer;
}
