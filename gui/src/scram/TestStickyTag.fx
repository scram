package scram;

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.effect.*;
import javafx.ext.swing.*;
import javafx.input.MouseEvent;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;

import java.lang.System;

SwingFrame {
    title: "Simple (JavaFX demo)"
    width: 500
    height: 500
    content: Canvas {
      content: Group {
          content: [
              StickyTag {
//                scaleX:0.5 scaleY:0.5
                dataObject: TestData.getDataObject()
                layoutObject: TestData.getLayoutObject()
              }
          ]
      }
    }
    visible: true  
  }
