/*
 * LayoutField.fx
 *
 * Created on Oct 29, 2008, 3:08:40 PM
 */

package scram;

public class LayoutField {
    public attribute name: String;
    public attribute x: Integer;
    public attribute y: Integer;
    public attribute width: Integer;
    public attribute height: Integer;
}
