/*
 * DataObject.fx
 *
 * Created on Oct 29, 2008, 5:54:10 PM
 */

package scram;

/**
 * @author jhn
 */

public class DataObject {
    attribute map: java.util.Map = new java.util.HashMap();
    
    public function put(key:String, value:String):Void {
        map.put(key, value)
    }
    
    public function get(key:String):String {
        map.get(key).toString()
    }
}
