/*
 * LayoutObject.fx
 *
 * Created on Oct 29, 2008, 3:05:34 PM
 */

package scram;

import javafx.scene.paint.Color;

/**
 * @author jhn
 */

public class LayoutObject {
    public attribute color: Color = Color.YELLOW;
    public attribute nameShown: Boolean;
    public attribute fields: LayoutField[];
}
