/*
 * NewSprintButton.fx
 *
 * Created on Oct 30, 2008, 10:29:17 AM
 */

package scram;

/**
 * @author recht
 */

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.image.*;
import javafx.scene.effect.*;
import javafx.scene.transform.*;
import javafx.ext.swing.*;
import javafx.scene.effect.*;
import javafx.scene.effect.light.*;
import javafx.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.animation.*;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class NewSprintButton extends CustomNode {
    public attribute action : function(:String) : Boolean;
    public attribute columns : Integer = 20;
    
        private attribute text : TextField = TextField {
            editable: true
            visible: false
            width: 0
            action: function() {
                if (action(text.text)) {
                    t.start();
                }
            }
        }
        private attribute t = Timeline {
            autoReverse: false
            toggle: true

            keyFrames: [
                KeyFrame {
                    action: function() {
                        text.visible = not text.visible;
                    }
                    time: 0s
                    values: text.columns => 0
                },
                KeyFrame {
                    time: 200ms
                    values: text.columns => columns tween Interpolator.EASEIN
                    action: function() {
                        text.jComponent.requestFocus();
                    }
                }
            ]
        };

    public function create():Node { 
        
        
        return Group {
            content: [ 
                Polyline {
                    translateY: 6
                    points : [ 4,0, 8,0, 8,4, 12,4, 12,8, 8,8, 8,12, 4,12, 4,8, 0,8, 0,4, 4,4, 4,0 ]
                    strokeWidth: 1.0
                    fill: Color.GREEN
                    stroke: Color.GREEN
                    cursor: Cursor.HAND
                    onMouseClicked: function(e) {
                        text.text = "";
                        t.start();
                    }
                    effect: DropShadow { radius: 2}
                },
                ComponentView {
                    translateX: 16
                    component: text
                }
            ]
        }

    }
}
