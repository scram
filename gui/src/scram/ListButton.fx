/*
 * ListButton.fx
 *
 * Created on Oct 29, 2008, 4:49:37 PM
 */

package scram;

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.effect.*;
import javafx.ext.swing.*;
import javafx.scene.effect.*;
import javafx.scene.effect.light.*;
import javafx.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.animation.*;

/**
 * @author recht
 */

public class ListButton extends CustomNode {
    public attribute left : Number = 10;
    public attribute text : String;
    
    private attribute t = Timeline {
        repeatCount: 1
        autoReverse: false
        toggle: true

        keyFrames: [
            KeyFrame {
                time: 0s
                values: left => 10
            },
            KeyFrame {
                time: 150ms
                values: left => 40 tween Interpolator.EASEIN
            }
        ]
    }

    public function ListButton(left: Number) {
        this.left = left;
    }
    
    public function create():Node { 
        return Group {
            content: [
                    Rectangle {
                        x: bind left y: 0
                        height: 20 width: 200
                        cursor: Cursor.HAND
                        onMouseEntered: function(e) {
                            t.start();
                        }
                        onMouseExited: function( e: MouseEvent ):Void {
                            t.start();
                        }
                        onMouseClicked: function(e) {
                            t.stop();
                            left = 10;
                        }
                        fill: Color.WHITE
                        effect: DropShadow { 
                            radius: 10 
                            color: Color.BLACK
                        }
                    },
                    Rectangle {
                        x: bind left + 10 y: 5
                        height: 10
                        width: 10
                        fill: Color.YELLOW
                        effect: DropShadow { radius: 3, color: Color.BLACK  }
                    },
                    Text {
                        cursor: Cursor.HAND
                        x: bind left + 30 y: 15
                        content: text
                        font: Font { 
                            name: "Arial"
                            size: 15 
                            style: FontStyle.BOLD
                        }
                    }]
        }
    }
}
