/*
 * Listener.fx
 *
 * Created on Oct 29, 2008, 9:56:34 PM
 */

package scram;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javafx.ext.swing.SwingFrame;

/**
 * @author recht
 */

public class Listener extends ComponentAdapter {
    public attribute size : WindowSize;
    public attribute frame : SwingFrame;

    public function componentShown(e: ComponentEvent) : Void {
        componentResized(e);
    }
    
    public function componentResized(e: ComponentEvent) : Void {
        size.height = frame.height;
        size.width = frame.width;
        size.x = frame.x;
        size.y = frame.y;
    }
}
