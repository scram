/*
 * StickyTag.fx
 *
 * Created on Oct 29, 2008, 2:38:00 PM
 */

package scram;

import javafx.scene.*;
import javafx.scene.geometry.*;
import javafx.scene.paint.*;
import javafx.input.MouseEvent;
import javafx.animation.*;
import javafx.scene.effect.*;
import  javafx.ext.swing.*;
import  java.awt.Font;

public class StickyTag extends CustomNode {
    public attribute layoutObject: LayoutObject;
    public attribute dataObject: DataObject;
    public attribute board : scram.pages.BoardNode;
    public attribute onMove : function(:DataObject, :String) : Void;
    
    attribute w = 300;
    attribute h = 300;
    attribute stX = 0.0;
    attribute stY = 0.0;
    attribute offsetX = 0.0;
    attribute offsetY = 0.0;
    attribute scale = 1.0;
    private attribute t = Timeline {
        repeatCount: 1
        autoReverse: false
        toggle: true

        keyFrames: [
            KeyFrame {
                time: 0s
                values: scale => 1.0
            },
            KeyFrame {
                time: 150ms
                values: scale => 2.0 tween Interpolator.EASEBOTH
            }
        ]
    }

    public function create(): Node {
        return Group {
            scaleX: bind scale scaleY: bind scale
            anchorX:w/2 anchorY:h/2
            content: [
                Rectangle {
                    x: 8 y: 8
                    width: w height: w
                    fill: Color.GRAY opacity:0.5
                    arcWidth: 5 arcHeight: 5
                },
                Rectangle {
                    x: 0 y: 0
                    width: w height: h
                    fill: layoutObject.color
                    arcWidth: 5 arcHeight: 5
                },
                for(field in layoutObject.fields) {
                    getNode(field, layoutObject)
                }
            ]
            var hover : Status;
            var f : Paint;
            var dragged : Boolean = false;
            
            onMouseDragged: function( e: MouseEvent ):Void {
                if( e.getX() < 1000 and e.getX() > -1000 ) {
                    stX = e.getDragX()/scaleX + offsetX;
                    stY = e.getDragY()/scaleY + offsetY;
                    var r = board.rectangleForPoint(e.getScreenX(), e.getScreenY());
                    if (hover != r) {
                        if (hover != null) hover.rectangle.fill = f;
                        f = r.rectangle.fill;
                        r.rectangle.fill = Color.LIGHTGRAY;
                        hover = r;
                    } else if (hover == r) {
                    } else if (hover != null) {
                        hover.rectangle.fill = f;
                    }
                    dragged = true;
                }
            }
            onMouseReleased: function(e) {
                if (hover != null) {
                    if (dragged) {
                        if (onMove != null) {
                            onMove(dataObject, hover.status);
                        }
                    }
                    hover.rectangle.fill = f;
                    hover = null;
                }
                dragged = false;
            }
            onMousePressed: function( e: MouseEvent ):Void {
                offsetX = stX;
                offsetY = stY; 
            }
            onMouseClicked: function( e: MouseEvent ):Void {
                t.start();
            }
            translateX: bind stX
            translateY: bind stY
        }
    }
    
    function getNode(layoutField: LayoutField, layoutObject: LayoutObject): Node {
        var view = new javax.swing.JTextPane();

        view.setContentType("text/html");
        view.setText(dataObject.get(layoutField.name));
        view.setBackground(layoutObject.color.awtColor);
        view.setEditable(false);
        var sizeRect = new java.awt.Dimension(layoutField.width, layoutField.height);

        var pane = new javax.swing.JScrollPane(view);
        pane.setPreferredSize(sizeRect);
        pane.setBorder(null);

        ComponentView {
            component: Component.fromJComponent(pane);
            translateX:layoutField.x
            translateY:layoutField.y
        }
    }
}