/*
 * Status.fx
 *
 * Created on Oct 30, 2008, 1:32:32 PM
 */

package scram;

import javafx.scene.geometry.Rectangle;

/**
 * @author recht
 */

public class Status {
    public attribute status : String;
    public attribute rectangle : Rectangle;

}
