/*
 * PageIcon.fx
 *
 * Created on Oct 30, 2008, 9:18:38 AM
 */

package scram;

/**
 * @author recht
 */
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.image.*;
import javafx.scene.effect.*;
import javafx.scene.transform.*;
import javafx.ext.swing.*;
import javafx.scene.effect.*;
import javafx.scene.effect.light.*;
import javafx.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.animation.*;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class PageIcon extends CustomNode {
    public attribute height : Number;
    public attribute savedComponent : Component;
    public attribute action : function(:Component) : Void;

    public function create():Node { 
        var point = new java.awt.Point();
        javax.swing.SwingUtilities.convertPointToScreen(point, savedComponent.getJComponent());
        var w : java.awt.Window = javax.swing.SwingUtilities.windowForComponent(savedComponent.jComponent);
    
        var robot : Robot = new Robot();
        var screenShot : BufferedImage = robot.createScreenCapture(new Rectangle(point.x, point.y, w.getWidth(), w.getHeight() - 100));
    

        return ImageView {
        var scale = height / w.getWidth();
        var trans = Transform.scale(scale, scale)
        
        var t = Timeline {
            autoReverse: false
            toggle: true

            keyFrames: [
                KeyFrame {
                    time: 0s
                    values: [trans.x => scale, trans.y => scale ]
                },
                KeyFrame {
                    time: 100ms
                    values: [ trans.x => scale * 1.5 tween Interpolator.EASEIN, 
                        trans.y => scale * 1.5 tween Interpolator.EASEIN ]
                }
            ]
        }


        width: height
        height: height
        image: Image.fromBufferedImage(screenShot)
        transform: bind trans
        
        onMouseEntered: function(e) {
            t.start();
        }
        onMouseExited: function(e) {
            t.start();
        }
        onMouseClicked: function(e) {
            action(savedComponent);
            savedComponent.visible = true;
        }
        
    };
        
    }
}
