/*
 * ScramPlayer.fx
 *
 * Created on Oct 29, 2008, 9:58:25 PM
 */

package scram.fun;

import javafx.application.Frame;
import javafx.application.Stage;
import javafx.scene.media.*;
import javafx.scene.paint.Color;
import javafx.scene.geometry.*;
import javafx.input.*;

var player = MediaPlayer {
    autoPlay: false
    media: Media {
        source: "file:///Users/jhn/night.mp3"
    }
};

Frame {
    title: "ScramPlayer"
    width: 200
    height: 200
    closeAction: function() { 
        java.lang.System.exit( 0 ); 
    }
    visible: true

    stage: Stage {
        content: [MediaView {
            mediaPlayer: player
        },
        Rectangle {
           width: 100
           height: 50
           fill: Color.GRAY
           onMousePressed: function(e:MouseEvent):Void {
               player.play();
           }
        }
        ]
    }
}