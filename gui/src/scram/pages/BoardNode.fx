/*
 * BoardNode.fx
 *
 * Created on Oct 29, 2008, 10:31:00 PM
 */

package scram.pages;

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.scene.geometry.Rectangle;
import javafx.lang.Sequences;

import scram.*;

/**
 * @author jhn
 */

public class BoardNode extends CustomNode {
    public attribute sprintName: String;
    public attribute stati: String[];
    public attribute size : WindowSize;
    private attribute rectangles : Status[] = [];
    
    public function create(): Node {
        return Group {
            
            content: [
                Rectangle {
                    x:0 y:0 width: bind size.width height: bind size.height-50
                    fill: LinearGradient {
                        startX: 0.0
                        startY: 0.0
                        endX: 1.0
                        endY: 1.0
                        proportional: true
                        stops: [
                           Stop { offset: 0.0 color: Color.WHITE },
                           Stop { offset: 1.0 color: Color.GRAY }
                        ]
                    }
                }, 
                Rectangle {
                    x:10 y:10 width:bind size.width-20 height:30
                    stroke: Color.BLACK
                }, 
                Text {
                    content: bind sprintName
                    x:15 y:25
                    stroke: Color.BLACK
                }, 
                for(status in stati) {
                            var r = Rectangle {
                                y: 5
                                width: bind (size.width/stati.size()-20 )
                                height: bind size.height - 170
                                stroke: Color.BLACK
                            }
                            insert Status { rectangle: r status: status} into rectangles;
                    Group {
                        content: [
                            r,
                            Text {
                                content: status
                                stroke: Color.BLACK
                            }
                        ]
                        translateX: bind (Sequences.indexOf(stati,status)* size.width/stati.size()+10) 
                        translateY: 55

                    }
                }
            ]
        };
    }
    
    public function rectangleForPoint(x: Integer, y: Integer) : Status {
        for (s : Status in rectangles) {
            var point = new java.awt.Point();
            var rec = s.rectangle;
            scram.GUIUtils.getScreenCoordinates(rec, point);
            if (point.x < x and point.y < y and rec.width + point.x > x and rec.height + point.y > y) {
                return s;
            }
        }
        return null;
    }
}