package scram.pages;

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.effect.*;
import javafx.ext.swing.*;
import javafx.input.MouseEvent;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;

import java.lang.System;

import scram.*;

var frame = SwingFrame {
    var stX = 0.0
    var stY = 0.0
    var board = BoardNode {
      sprintName: "Dummy sprint"
      size: GUIUtils.getWindowSize()
      stati: ["To do","In progress","Completed"]
    }    

    title: "Simple (JavaFX demo)"
    content: Canvas {
      content: Group {
          content: [
                board,
                TestData.getStickyTags(3, board)
          ]
      }
    }
    visible: true  
};

var w=frame.getJFrame();
GUIUtils.makeFullScreen(w);