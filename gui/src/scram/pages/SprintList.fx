
/*
 * SprintList.fx
 *
 * Created on Oct 29, 2008, 6:15:02 PM
 */

package scram.pages;

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.effect.*;
import javafx.ext.swing.*;
import javafx.scene.effect.*;
import javafx.scene.effect.light.*;
import javafx.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.animation.*;
import scram.ListButton;
import javax.swing.JComponent;
import scram.*;

public class SprintList extends CustomNode {
    public attribute sprints : String[];
    public attribute assign : function(:Component):Void;
    public attribute size : WindowSize;

    public function create():Node { 
        var list : Node[] = [
            Rectangle { height: 20 },
            for (e in sprints) {
                ListButton {
                    left: 10
                    text: e
                    onMouseClicked: function(ev) {
                        selectSprint(e);
                    }
                }
            },
            scram.NewSprintButton {
                translateX: 10
                translateY: -10
                columns: 16
                action: function(s: String) {
                    java.lang.System.out.println("New sprint {s}");
                            
                    var b = ListButton {
                        left: 10
                        text: s
                        onMouseClicked: function(ev) {
                            selectSprint(s);
                        }
                    };
                    insert b before list[sizeof list - 1];
                    return true;
                }
            }

        ];
        return Group {             
            content: [
                Text {
                    y: 20
                    x: 10
                    content: "Scram: Select sprint"
                    font: Font {
                        size: 20
                        style: FontStyle.BOLD
                    }
                },

                VBox {
                    translateY: 10
                    spacing: 5
                    content: bind list
                }
            ]
        }
    }
    
    function selectSprint(sprint: String) {
                    var node = BoardNode {
                          sprintName: sprint
                          stati: ["To do","In progress","Completed"]
                          size: size
                    };
        assign(Canvas {
                content: [
                    node,
                    TestData.getStickyTags(3, node)
                ]
          })
    }
            
    public static function create(sprintList : String[], assignFunction : function(:Component):Void, size : WindowSize) {
        return Canvas { 
            background: Color.WHITE
            content: SprintList {
                sprints: sprintList
                assign: assignFunction
                size: size
            }
        };
    }
}
