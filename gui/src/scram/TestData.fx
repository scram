/*
 * TestData.fx
 *
 * Created on Oct 30, 2008, 10:24:49 AM
 */

package scram;

import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.geometry.*;
import javafx.scene.text.*;
import javafx.scene.effect.*;
import javafx.ext.swing.*;
import javafx.input.MouseEvent;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;

/**
 * @author jhn
 */

public class TestData {

    public static function getStickyTags(num:Integer, node: scram.pages.BoardNode): Node[] {
        for(s in [1..num]) {
          StickyTag {
            translateX: s*100
            translateY: s*100
            scaleX: 0.8 scaleY: 0.8
            dataObject: getDataObject()
            layoutObject: getLayoutObject()
            board: node
            onMove: function(obj: DataObject, status: String) {
                java.lang.System.out.println("Moved {obj} to {status}");
            }
          }
        }
    }


    public static function getDataObject(): DataObject {
        var data = new DataObject();
        data.put("name","<b>Design af gul seddel</b>");
        data.put("description","<html>Der skal laves en <b>gul seddel</b> komponent. Den skal " +
            "bla bla bla bla bla bla bla bla bla bla " +
            "bla bla bla bla bla bla bla bla bla bla bla bla " +
            "bla bla bla bla bla bla bla bla bla bla " +
            "bla bla bla bla bla bla bla bla bla bla bla bla " +
            "bla bla bla bla bla bla bla bla bla bla bla bla " +
            "bla bla bla bla bla bla.</html>");
        data.put("estimate","5 timer");
        data;
    }

    public static function getLayoutObject(): LayoutObject {
        LayoutObject {
            color: Color.web("#fff56d", 1.0)
            fields: [
                LayoutField {
                    x:10 y:10 width:280 height: 25
                    name:"name"
                },
                LayoutField {
                    x:10 y:40 width:280 height: 200
                    name:"description"
                },
                LayoutField {
                    x:10 y:270 width:280 height: 25
                    name:"estimate"
                }
            ]
        }
    }
}
