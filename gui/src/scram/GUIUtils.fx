/*
 * GUIUtils.fx
 *
 * Created on Oct 29, 2008, 11:19:25 PM
 */

package scram;

import java.awt.GraphicsEnvironment;
import javafx.scene.Node;

/**
 * @author jhn
 */

public class GUIUtils {

    public static function getWindowSize():WindowSize {
        WindowSize {
            width: GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width
            height: GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height
        }
    }
    
    public static function screenBounds():java.awt.Rectangle {
        GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
    }
    
    public static function makeFullScreen(w:java.awt.Window): Void {
        GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(w);
    }
    
    public static function getScreenCoordinates(node: Node, p : java.awt.Point): Void {
        var parent : Node = node.parent;
        if (parent == null) {
            return;
        }
        p.x = parent.getBoundsX().intValue() + p.x;
        p.y = parent.getBoundsY().intValue() + p.y;
        
        getScreenCoordinates(parent, p);
        
    }
}
